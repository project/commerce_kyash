CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

-- INTRODUCTION --

The India Commerce Kyash module provide a payment method for Drupal
commerce to Interact with Kyash payment gateway.


-- REQUIREMENTS --

Drupal Commerce(https://www.drupal.org/project/commerce)


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.
* Goto admin/commerce/config/payment-methods 
* enable a rule for Kyash Payment Gateway.
* Edit Action and Configure.
